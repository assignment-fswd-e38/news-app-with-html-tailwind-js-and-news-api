const articlesdiv = document.getElementById('articlediv')
const API_KEY = import.meta.env.VITE_API_KEY
const API_URL = (`https://gnews.io/api/v4/top-headlines?category=general&max=4&country=in&apikey=${API_KEY}`)
// Function to fetch data from API
async function fetchData() {
    try {
        const response = await fetch(API_URL);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const data = await response.json();
        console.log(data); // Log the fetched data directly without assigning it to a variable
        return data; // Return the fetched data
    } catch (error) {
        console.error('Error fetching data:', error);
        return null;
    }
}

// Call the fetchData function and log the result
fetchData()
    .then(data => {
        // Limit the array length to 5
        const limitedArray = data.articles.slice(0, 5);
        console.log(limitedArray); // Log the first 5 articles

        const articlesdiv =document.getElementById('articlediv')

        // Array1

        const article1 =document.createElement('article')
        article1.classList.add("lg:row-start-1")
        article1.classList.add("row-span-3")
        articlesdiv.appendChild(article1)
        const image1 =document.createElement('img')
        image1.classList.add("w-full")
        if (limitedArray[0].image) {
            image1.src = limitedArray[0].image;
        } else {
            image1.src = 'https://th.bing.com/th/id/OIP.-7H_BmzNXiraSflf1Qd6_gHaEK?rs=1&pid=ImgDetMain';
        }
        article1.appendChild(image1)
        const content1=document.createElement('div')
        article1.appendChild(content1)
        const heading1=document.createElement('h2')
        heading1.classList.add("text-4xl")
        heading1.classList.add("font-bold")
        heading1.classList.add("mt-5")
        heading1.classList.add("mb-10")
        heading1.innerHTML= limitedArray[0].title
        content1.appendChild(heading1)
        const description1=document.createElement('p')
        description1.innerHTML= limitedArray[0].description
        content1.appendChild(description1)

        // Array 2

        const article2 =document.createElement('article')
        article2.classList.add("flex")
        article2.classList.add("gap-5")
        articlesdiv.appendChild(article2)
        const image2 =document.createElement('img')
        image2.classList.add("w-1/4")
        if (limitedArray[1].image) {
            image2.src = limitedArray[1].image;
        } else {
            image2.src = 'https://th.bing.com/th/id/OIP.-7H_BmzNXiraSflf1Qd6_gHaEK?rs=1&pid=ImgDetMain';
        }
        article2.appendChild(image2)
        const content2=document.createElement('div')
        article2.appendChild(content2)
        const heading2=document.createElement('h2')
        heading2.classList.add("text-lg")
        heading2.classList.add("font-bold")
        heading2.classList.add("mb-2")
        heading2.innerHTML= limitedArray[1].title
        content2.appendChild(heading2)
        const description2=document.createElement('p')
        description2.innerHTML= limitedArray[1].description
        content2.appendChild(description2)

         // Array 3

        const article3 =document.createElement('article')
        article3.classList.add("flex")
        article3.classList.add("gap-5")
        articlesdiv.appendChild(article3)
        const image3 =document.createElement('img')
        image3.classList.add("w-1/4")
        if (limitedArray[2].image) {
            image3.src = limitedArray[2].image;
        } else {
            image3.src = 'https://th.bing.com/th/id/OIP.-7H_BmzNXiraSflf1Qd6_gHaEK?rs=1&pid=ImgDetMain';
        }
        article3.appendChild(image3)
        const content3=document.createElement('div')
        article3.appendChild(content3)
        const heading3=document.createElement('h2')
        heading3.classList.add("text-lg")
        heading3.classList.add("font-bold")
        heading3.classList.add("mb-2")
        heading3.innerHTML= limitedArray[2].title
        content3.appendChild(heading3)
        const description3=document.createElement('p')
        description3.innerHTML= limitedArray[2].description
        content3.appendChild(description3)

        // Array 4

        const article4 =document.createElement('article')
        article4.classList.add("flex")
        article4.classList.add("gap-5")
        articlesdiv.appendChild(article4)
        const image4 =document.createElement('img')
        image4.classList.add("w-1/4")
        if (limitedArray[3].image) {
            image4.src = limitedArray[3].image;
        } else {
            image4.src = 'https://th.bing.com/th/id/OIP.-7H_BmzNXiraSflf1Qd6_gHaEK?rs=1&pid=ImgDetMain';
        }
        article4.appendChild(image4)
        const content4=document.createElement('div')
        article4.appendChild(content4)
        const heading4=document.createElement('h2')
        heading4.classList.add("text-lg")
        heading4.classList.add("font-bold")
        heading4.classList.add("mb-2")
        heading4.innerHTML= limitedArray[3].title
        content4.appendChild(heading4)
        const description4=document.createElement('p')
        description4.innerHTML= limitedArray[3].description
        content4.appendChild(description4)
    })
    .catch(error => console.error('Error:', error));

    

